#!/usr/bin/env bash

set -euo pipefail

if [ -n "${APT_PROXY_URL:-}" ]; then
  echo "Acquire::http::Proxy \"$APT_PROXY_URL\";" > /etc/apt/apt.conf.d/00aptproxy
fi

apt-get update -y && apt-get upgrade -y

apt-get install -y \
    git \
    jq \
    curl \
    sudo \
    software-properties-common \
    unzip \
    zip \
    htop \
    vim

add-apt-repository ppa:ondrej/php

apt-get update -y

apt-get install -y \
    "php${PHP_VERSION}-cli" \
    "php${PHP_VERSION}-curl" \
    "php${PHP_VERSION}-bcmath" \
    "php${PHP_VERSION}-amqp" \
    "php${PHP_VERSION}-apcu" \
    "php${PHP_VERSION}-memcached" \
    "php${PHP_VERSION}-ldap" \
    "php${PHP_VERSION}-bz2" \
    "php${PHP_VERSION}-mbstring" \
    "php${PHP_VERSION}-imagick" \
    "php${PHP_VERSION}-gd" \
    "php${PHP_VERSION}-imap" \
    "php${PHP_VERSION}-intl" \
    "php${PHP_VERSION}-mysql" \
    "php${PHP_VERSION}-pgsql" \
    "php${PHP_VERSION}-sqlite3" \
    "php${PHP_VERSION}-redis" \
    "php${PHP_VERSION}-gmp" \
    "php${PHP_VERSION}-mongodb" \
    "php${PHP_VERSION}-xdebug" \
    "php${PHP_VERSION}-xmlrpc" \
    "php${PHP_VERSION}-soap" \
    "php${PHP_VERSION}-xsl" \
    "php${PHP_VERSION}-zip"

rm -f /etc/apt/apt.conf.d/00aptproxy
