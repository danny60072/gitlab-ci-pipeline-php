# gitlab-ci-pipeline-php

Gitlab Repo: https://gitlab.com/danny60072/gitlab-ci-pipeline-php

修改 https://github.com/edbizarro/gitlab-ci-pipeline-php 而來

use kaniko build image for CI

For tag list: https://hub.docker.com/r/danny60072/gitlab-ci-pipeline-php/tags

## PHP Version
- 7.4
- 8.0
- 8.1
- 8.2
- 8.3

## Javascript
- Node.js
  - 14
  - 16
  - 18
  - 20
- yarn 1
