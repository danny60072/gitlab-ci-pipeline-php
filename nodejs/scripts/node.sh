#!/usr/bin/env bash

set -euo pipefail

curl -fsSL https://deb.nodesource.com/setup_"${NODE_VERSION:-14}".x | sudo -E bash - && \
apt-get install -y nodejs

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

apt-get update -y && apt-get install -y yarn
