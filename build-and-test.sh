#!/usr/bin/env bash

set -euo pipefail

NODE_VERSION=16
APT_PROXY_URL=http://192.168.1.8:3142

rm -f gitlab-ci-pipeline-php-8.3-testbuild.tar
rm -f gitlab-ci-pipeline-php-8.3-node"$NODE_VERSION"-testbuild.tar

docker run \
    --rm \
    -v "${PWD}":/workspace \
    gcr.io/kaniko-project/executor:v1.22.0-debug \
    --dockerfile /workspace/php/8.3/Dockerfile \
    --destination "danny60072/gitlab-ci-pipeline-php:8.3-testbuild" \
    --build-arg APT_PROXY_URL="${APT_PROXY_URL:-}" \
    --context dir:///workspace/ \
    --no-push \
    --tarPath=gitlab-ci-pipeline-php-8.3-testbuild.tar | tee output-1.log

docker load --input gitlab-ci-pipeline-php-8.3-testbuild.tar
docker push danny60072/gitlab-ci-pipeline-php:8.3-testbuild

docker run \
    --rm \
    -v "${PWD}":/workspace \
    gcr.io/kaniko-project/executor:v1.22.0-debug \
    --dockerfile /workspace/nodejs/Dockerfile \
    --destination "danny60072/gitlab-ci-pipeline-php:8.3-node$NODE_VERSION-testbuild" \
    --build-arg PHP_VERSION="8.3" \
    --build-arg NODE_VERSION="$NODE_VERSION" \
    --build-arg APT_PROXY_URL="${APT_PROXY_URL:-}" \
    --context dir:///workspace/ \
    --no-push \
    --tarPath=gitlab-ci-pipeline-php-8.3-node$NODE_VERSION-testbuild.tar | tee output-2.log

docker load --input gitlab-ci-pipeline-php-8.3-node$NODE_VERSION-testbuild.tar

docker run -it --rm \
    -v "${PWD}":/var/www/html \
    --env NODE_VERSION="$NODE_VERSION" \
    danny60072/gitlab-ci-pipeline-php:8.3-node$NODE_VERSION-testbuild \
    goss -g tests/goss-8.3.yaml v
